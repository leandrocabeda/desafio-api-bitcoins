package com.bitcoins.api.responses;

import java.util.ArrayList;
import java.util.List;

import lombok.*;

@Data
public class Response<T> {

	
	private T data;
	private String message;
	private List<String> errors;
	

	public List<String> getErrors() {
		
		if(this.errors==null)
		{
			this.errors= new ArrayList<String>();
		}
		
		return errors;
	}

	
	
}
