package com.bitcoins.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
@EnableCaching
public class ApiBitcoinsApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiBitcoinsApplication.class, args);
	}

}
