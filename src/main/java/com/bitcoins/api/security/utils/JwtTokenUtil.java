package com.bitcoins.api.security.utils;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Component
public class JwtTokenUtil {
	
	static final String CLAIN_KEY_USERNAME="sub"; 
	static final String CLAIN_KEY_ROLE="role";
	static final String CLAIN_KEY_CREATED="created";
	
	private String secret="_@HRL&L3eaksdGHE%AS#$FSG324sJ3_";
	
	private Long expiration=(long) 604800;
	
	
	
	/* 
	 * 
	 * @param userDetails
	 * @return String
	 * 
	 * */
	public String getToken(UserDetails userDetails)
	{
		Map<String, Object> claims = new HashMap<>();
		claims.put(CLAIN_KEY_USERNAME,userDetails.getUsername());
		//userDetails.getAuthorities().forEach(authority -> claims.put(CLAIN_KEY_ROLE,authority.getAuthority()));
		claims.put(CLAIN_KEY_CREATED, new Date());
		
		return generateToken(claims);
	}
	
	
	/*
	 * 
	 * @param claims
	 * @return String
	 * 
	 * */
	private String generateToken(Map<String,Object> claims)
	{
		return Jwts.builder().setClaims(claims).setExpiration(generateDataExpiration())
				.signWith(SignatureAlgorithm.HS512,secret).compact();
	}
	
	
	/* 
	 * 
	 * @return Date
	 * 
	 * */
	public Date generateDataExpiration()
	{
		// Pega o tempo atual e soma com "expiration" , nisso joga 7 dias para frente de novo
		return new Date(System.currentTimeMillis()+expiration*1000);
	}
	
	
	
	/*
	 * 
	 * @param token
	 * @return boolean
	 * */
	public boolean tokenValidate(String token)
	{
		return !tokenExpiration(token);
	}
	
	
	
	/* 
	 * 
	 * @param token
	 * @return boolean
	 * 
	 * */
	private boolean tokenExpiration(String token)
	{
		Date dataExpiration = this.getExpirationDateFromToken(token);
		
		if(dataExpiration == null)
		{
			return false;
		}

		return dataExpiration.before(new Date());
	}
	
	
	
	/* 
	 * 
	 * @param token
	 * @return Date
	 * 
	 * */
	public Date getExpirationDateFromToken(String token) 
	{
		Date expiration;
		
		try {
			
			Claims claims = getClaimsFromToken(token);
			expiration = claims.getExpiration();
			
		} catch (Exception e) {
			expiration = null;
		}
		
		return expiration;
		
	}
	
	
	/* 
	 * 
	 * @token 
	 * @return String
	 * */
	public String refreshToken(String token)
	{
		String refreshedToken;
		
		try {
			
			Claims claims = getClaimsFromToken(token);
			claims.put(CLAIN_KEY_CREATED, new Date());
			
			refreshedToken = generateToken(claims);
			
		} catch (Exception e) {
			refreshedToken=null;
		}
		
		return refreshedToken;
	}
	

	/*
	 * @param token
	 * @return Claims
	 * */
	private Claims getClaimsFromToken(String token)
	{
		Claims claims;
		
		try {
			
			claims = Jwts.parser().setSigningKey(secret).parseClaimsJws(token).getBody();
			
		} catch (Exception e) {
			claims= null;
		}
		
		return claims;
	}
	
	
	
	
	/* 
	 * @param token
	 * @return String
	 * 
	 * */
	public String getUsernameFromToken(String token)
	{
		String username;
		
		try {
			
			Claims claims = getClaimsFromToken(token);
			username = claims.getSubject();
			
		} catch (Exception e) {
			
			username=null;
		}
		
		return username;
	}

}
