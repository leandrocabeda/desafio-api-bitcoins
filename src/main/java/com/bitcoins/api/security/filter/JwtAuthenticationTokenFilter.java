package com.bitcoins.api.security.filter;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import com.bitcoins.api.security.utils.JwtTokenUtil;

import io.jsonwebtoken.ExpiredJwtException;

@Component
public class JwtAuthenticationTokenFilter extends OncePerRequestFilter {
	
	private static final String AUTH_HEADER="Authorization";
	private static final String BEARER_PREFIX="Bearer";
	
	private static final Logger log= LoggerFactory.getLogger(JwtAuthenticationTokenFilter.class);
	
	@Autowired
	@Qualifier("userDetailsService") 
	private UserDetailsService userDetailsService;
	
	@Autowired
	private JwtTokenUtil jwtTokenUtil;
	
	
	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
			throws ServletException, IOException {
		
		String token = request.getHeader(AUTH_HEADER);
		String username=null;
		
		if(token!=null && token.startsWith(BEARER_PREFIX))
		{
			log.info("substraing token with username");
			
			token= token.substring(7);
			
			try {

				username = jwtTokenUtil.getUsernameFromToken(token);

			} catch (IllegalArgumentException e) {

				System.out.println("Error generate token JWT");

			} catch (ExpiredJwtException e) {

				System.out.println("Error Token JWT expiration");
			}
			
		}
		
		
		
		if(username != null && SecurityContextHolder.getContext().getAuthentication() == null)
		{
			UserDetails userDetails = userDetailsService.loadUserByUsername(username);
			
			if(jwtTokenUtil.tokenValidate(token))
			{
				log.info("Validate token username");
				UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken (
						userDetails,null,userDetails.getAuthorities());
				
				authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
				SecurityContextHolder.getContext().setAuthentication(authentication);
				
			}
		}
		
		chain.doFilter(request, response);
		
	}

}
