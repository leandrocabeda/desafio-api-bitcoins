package com.bitcoins.api.security.dto;

import java.util.Date;

import lombok.*;

@Data
public class TickerBitcoins {
	
	
	private String hight;
	private String low;
	private String vol;
	private String last;
	private String buy;
	private String sell;
	private String open;
	private Date data;

}
