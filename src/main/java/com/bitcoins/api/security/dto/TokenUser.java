package com.bitcoins.api.security.dto;

import lombok.*;

@Data
public class TokenUser {

	private String token;
	
	public TokenUser(String token)
	{
		this.token=token;
	}
	
}
