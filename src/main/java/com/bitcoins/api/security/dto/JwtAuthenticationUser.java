package com.bitcoins.api.security.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import com.bitcoins.api.entities.Clients;

import lombok.*;

@Data
public class JwtAuthenticationUser {

	
	@NotEmpty(message = "Login cannot be empty")
	@NotBlank(message = "Login cannot be blank")
	@NotNull(message = "Login cannot be null")
	@Length(min = 5,message = "Login must be at least {min} characters")
	private String login;
	
	@NotEmpty(message = "Password cannot be empty")
	@NotBlank(message = "Password cannot be blank")
	@NotNull(message = "Password cannot be null")
	@Length(min = 6,message = "Password must be at least {min} characters")
	private String password;
	
	public JwtAuthenticationUser()
	{
		
	}
	
	public JwtAuthenticationUser(String login, String password)
	{
		this.login=login;
		this.password=password;
	}

}
