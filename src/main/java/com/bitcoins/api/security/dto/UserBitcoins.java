package com.bitcoins.api.security.dto;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import lombok.*;

@Data
public class UserBitcoins {
	
	
	@NotEmpty(message = "Email cannot be empty")
	@NotBlank(message = "Email cannot be blank")
	@NotNull(message = "Email cannot be null")
	@Email(message = "Invalid email")
	private String email;
	

	@NotEmpty(message = "Value cannot be empty")
	@NotBlank(message = "Value cannot be blank")
	@NotNull(message = "Value cannot be null")
	private String value ;
	
	public UserBitcoins()
	{
		
	}
	
	
	public UserBitcoins(String email, String value)
	{
		this.email=email;
		this.value=value;
	}
	

}
