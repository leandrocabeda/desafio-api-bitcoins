package com.bitcoins.api.security.controller;



import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bitcoins.api.entities.Clients;
import com.bitcoins.api.responses.Response;
import com.bitcoins.api.security.dto.JwtAuthenticationUser;
import com.bitcoins.api.security.dto.TokenUser;
import com.bitcoins.api.security.utils.JwtTokenUtil;
import com.bitcoins.api.service.ClientsService;


@RestController
@RequestMapping("/auth")
@CrossOrigin(origins = "*") 
public class AuthenticationController {

	private static final Logger log= LoggerFactory.getLogger(AuthenticationController.class);
	
	@Autowired
	private JwtTokenUtil jwtTokenUtil;
	
	@Autowired
	private ClientsService serv;
	
	
	 @Autowired
	 @Qualifier("userDetailsService") 
	 private UserDetailsService userDetailsService;
	

	@PostMapping
	public ResponseEntity<Response<TokenUser>> generateTokenJwt(@Valid @RequestBody 
			JwtAuthenticationUser jwtUser, BindingResult result)
	{
		Response<TokenUser> response = new Response<TokenUser>();
		
		if(result.hasErrors())
		{
			log.error("Error validation : {}", result.getAllErrors());
			result.getAllErrors().forEach(error-> response.getErrors().add(error.getDefaultMessage()));
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
		}
		
		log.info("Generating the token for email {}.", jwtUser.getLogin());

		UserDetails userDetails = userDetailsService.loadUserByUsername(jwtUser.getLogin());
		String token = jwtTokenUtil.getToken(userDetails);
		
		response.setData(new TokenUser(token));
			
		return ResponseEntity.status(HttpStatus.OK).body(response);	
		
	}
	
	
	@PostMapping("/createClients")
	public ResponseEntity<Response<Clients>> createUser(@Valid @RequestBody 
			Clients clients, BindingResult result)
	{
		Response<Clients> response = new Response<Clients>();
		
		
		if(result.hasErrors())
		{
			log.error("Error validation : {}", result.getAllErrors());
			result.getAllErrors().forEach(error-> response.getErrors().add(error.getDefaultMessage()));
			
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
		}
		
		
		log.info("Verify if exists clients with login or email and create if not exists");
		if(serv.insert(clients))
		{
			response.getErrors().add("Exists clients with login or email, please verify!");
			
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
		}
		
		response.setData(clients);
		return ResponseEntity.status(HttpStatus.CREATED).body(response);	
		
	}
	
	
}
