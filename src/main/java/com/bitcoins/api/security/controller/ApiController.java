package com.bitcoins.api.security.controller;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bitcoins.api.responses.Response;
import com.bitcoins.api.security.dto.UserBitcoins;
import com.bitcoins.api.service.ClientsService;


@RestController
@RequestMapping("/auth/api")
public class ApiController {
	
	private static final Logger log= LoggerFactory.getLogger(ApiController.class);
	
	@Autowired
	private ClientsService serv;
	
	@GetMapping()
	public ResponseEntity<String> welcome()
	{
		log.info("URL API Main");
		return ResponseEntity.status(HttpStatus.OK).body("Welcome API Bitcoins");
		
	}
	
	
	@PostMapping("/insertDeposit")
	public ResponseEntity<Response<String>> insertDeposit(@Valid @RequestBody 
			UserBitcoins clients, BindingResult result)
	{
		Response<String> response = new Response<String>();
		
		
		if(result.hasErrors())
		{
			log.error("Error validation : {}", result.getAllErrors());
			result.getAllErrors().forEach(error-> response.getErrors().add(error.getDefaultMessage()));
			
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
		}
		
		
		log.info("Validating the customer deposit");
		String message = serv.insertDeposit(clients.getEmail(),clients.getValue());
		if(message.contains("Error!"))
		{
			response.getErrors().add(message);
			
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
		}
		
		response.setMessage(message);
		return ResponseEntity.status(HttpStatus.OK).body(response);	
		
	}
	
	
	@GetMapping("/checkBalance/{id}")
	public ResponseEntity<Response<String>> checkBalance(@PathVariable("id") Long id)
	{
		Response<String> response = new Response<String>();
		
		
		log.info("Consulting customer balance");
		String message = serv.checkBalance(id);
		
		if(message.contains("Error!"))
		{
			response.getErrors().add(message);
			
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
		}
		
		response.setMessage(message);
		return ResponseEntity.status(HttpStatus.OK).body(response);	
		
	}
	
	
	@GetMapping("/priceQuoteBitcoins")
	public ResponseEntity<Response<String>> priceQuoteBitcoins()
	{
		Response<String> response = new Response<String>();
		
		log.info("Checking Bitcoins Quotation");
		
		response.setMessage(serv.priceQuoteBitcoins());
		
		return ResponseEntity.status(HttpStatus.OK).body(response);	
		
	}
	
	
	@PostMapping("/buyBitcoins")
	public ResponseEntity<Response<String>> buyBitcoins(@Valid @RequestBody 
			UserBitcoins clients, BindingResult result)
	{
		Response<String> response = new Response<String>();
		
		if(result.hasErrors())
		{
			log.error("Error validation : {}", result.getAllErrors());
			result.getAllErrors().forEach(error-> response.getErrors().add(error.getDefaultMessage()));
			
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
		}
		
		
		
		log.info("Making the investment of bitcoins");
		String message = serv.buyBitcoins(clients.getEmail(),clients.getValue());
		if(message.contains("Error!"))
		{
			response.getErrors().add(message);
			
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
		}
		
		
		response.setMessage(message);
		
		return ResponseEntity.status(HttpStatus.OK).body(response);	
		
	}
	
	
	@GetMapping("/checkInvestment/{id}")
	public ResponseEntity<Response<String>> checkInvestment(@PathVariable("id") Long id)
	{
		Response<String> response = new Response<String>();
		
		
		log.info("Consulting customer investment");
		String message = serv.checkInvestment(id);
		
		if(message.contains("Error!"))
		{
			response.getErrors().add(message);
			
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
		}
		
		response.setMessage(message);
		return ResponseEntity.status(HttpStatus.OK).body(response);	
		
	}
	
	

}
