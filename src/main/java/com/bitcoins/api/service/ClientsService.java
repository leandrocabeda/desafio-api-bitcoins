package com.bitcoins.api.service;


import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.bitcoins.api.entities.Clients;
import com.bitcoins.api.entities.Historic;
import com.bitcoins.api.entities.Investments;
import com.bitcoins.api.repositories.ClientsRepository;
import com.bitcoins.api.repositories.HistoricRepository;
import com.bitcoins.api.repositories.InvestmentsRepository;
import com.bitcoins.api.security.dto.TickerBitcoins;

import io.jsonwebtoken.lang.Assert;

@Service
public class ClientsService {
	
	@Autowired
    private ClientsRepository userRep;
	
	@Autowired
	private HistoricRepository historicRep;
	
	@Autowired
	private InvestmentsRepository invesRep;
	
	
	@Autowired
    private JavaMailSender emailSender;
	
	@Autowired
	private RestTemplate rest;
	
	
	private SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
	
	
	@Bean
	public RestTemplate restTemplate() {
	    return new RestTemplate();
	}
	
	
	private ResponseEntity<TickerBitcoins> getTickerBitcoins(String url)
	{
		return rest.getForEntity(url, TickerBitcoins.class);
	}
	
	
	public boolean insert(Clients clients) {

		Assert.isNull(clients.getId(), "Could not insert register");
		Clients client=userRep.findByLoginOrEmail(clients.getLogin(), clients.getEmail());
		
		if(client!=null)
		{
			return true;
		}
		
		BCryptPasswordEncoder encode= new BCryptPasswordEncoder();
		
		clients.setPassword(encode.encode(clients.getPassword()));

		userRep.save(clients);
		
		return false;
		
	}
	
	public String insertDeposit(String email, String value) {

		Assert.notNull(email, "Could not insert register");
		Assert.notNull(value, "Could not insert register");
		
		Clients client= userRep.findByEmail(email);
		
		if(client == null)
		{
			return "Error! Customer email does not exist in the records for "
					+ "making the deposit, please insert a valid email";
		}
		
		
		String valueClient= client.getValue();

		client.setValue(
				valueClient==null?
						value:
						""+(Double.parseDouble(valueClient)+Double.parseDouble(value))
				);
		
		userRep.save(client);
		
		SimpleMailMessage msg = new SimpleMailMessage();
        msg.setTo(client.getEmail());
        msg.setText("The deposit amount was "+value);

        emailSender.send(msg);

		return "Deposit amount saved successfully!" +client;
		
	}
	
	
	public String checkBalance(Long id)
	{
		Assert.notNull(id, "Could not query register");
		
		Optional<Clients> clientOpt = userRep.findById(id);
		
		if(!clientOpt.isPresent())
		{
			return "Error! client not found with this Id: "+id;
		}
		
		
		return "The balance of the customer with a "+clientOpt.get().getName()
				+"  name is: "
			    + clientOpt.get().getValue();
		
	}
	
	
	public String priceQuoteBitcoins()
	{
		
		TickerBitcoins ticker = getTickerBitcoins("https://www.mercadobitcoin.net/api/BTC/ticker/")
													.getBody();
		
		historicRep.save(new Historic(ticker.getBuy(),ticker.getSell()));
		
		return "The current quote for Buy Bitcoins is "+ticker.getBuy()+"and Sale is "+ticker.getSell();
		
	}
	
	
	public String buyBitcoins(String email, String value)
	{
		
		Assert.notNull(email, "Could not insert buying");
		Assert.notNull(value, "Could not insert buying");
		
		Clients client= userRep.findByEmail(email);
		
		if(client == null)
		{
			return "Error! The customer's email does not exist in the records for " + 
					"purchase bitcoins, enter a valid email";
		}
		
		TickerBitcoins ticker = getTickerBitcoins("https://www.mercadobitcoin.net/api/BTC/ticker/")
				.getBody();
		
		
		Double valueTicker = Double.parseDouble(ticker.getBuy());
		Double valueTickerToReal = valueTicker*4.6;  // Value 4.6 real today
		String valueClient = client.getValue();
		
		
		if(Double.parseDouble(valueClient)<Double.parseDouble(value))
		{
			return "Error! The investment value is greater than the available balance";
		}
		else
		{
			if(Double.parseDouble(value) < valueTickerToReal)
			{
				return "Error! The investment value is less than the purchase price of Bitcoins";
			}
			
			int numInvest = (int)(Double.parseDouble(value)/valueTickerToReal);
			Double investTotal = (Double)(numInvest*valueTicker);
			
			Investments investment = new Investments();
			investment.setValueBuyBitcoins(""+investTotal);
			investment.setValueInvestments(value);
			investment.setData(new Date());
			investment.setClient(client);
			
			invesRep.save(investment);
			
			
			client.setValue(""+(Double.parseDouble(valueClient)-Double.parseDouble(value)));
			
			userRep.save(client);
			
			SimpleMailMessage msg = new SimpleMailMessage();
	        msg.setTo(client.getEmail());
	        msg.setText("The investment value was "+value+" and the compared value of Bitcoins was "+investTotal);
	        emailSender.send(msg);
			
		}

		return "Investment made successfully!";
		
	}
	
	
	public String checkInvestment(Long id)
	{
		Assert.notNull(id, "Could not query register");
		
		Investments investmentClient = invesRep.findByClient(id);
		
		if(investmentClient == null)
		{
			return "Error! client not found with this Id: "+id+" in table Investments";
		}
		
		
		return "The purchase date was "+format.format(investmentClient.getData())
				+ ", the investment amount was "+investmentClient.getValueInvestments()
				+ "  and the Bitcoins value on that day was "+investmentClient.getValueBuyBitcoins();
		
	}
	
	

}
