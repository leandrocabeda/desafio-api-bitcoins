package com.bitcoins.api.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.bitcoins.api.entities.Clients;
import com.bitcoins.api.repositories.ClientsRepository;

@Service(value = "userDetailsService") 
public class UserDetailsServiceImpl implements UserDetailsService {

	@Autowired
    private ClientsRepository userRep;
	
	
	 @Override
	    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		 
		 
	      Clients client = userRep.findByLogin(username);

	        if(client == null) {
	        	
	            throw new UsernameNotFoundException("Login client not found!");
	            
	        }

	        return client;
	       
	        
	        
	    }

}
