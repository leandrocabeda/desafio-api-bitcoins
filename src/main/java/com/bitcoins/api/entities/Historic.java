package com.bitcoins.api.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.*;

@Data
@Entity
public class Historic {
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(nullable = true)
	private String valueBuyBitcoins;
	
	
	@Column(nullable = true)
	private String valueSellBitcoins;
	
	@Column(name="data_historic")
	private Date data;
	
	
	public Historic(String valueBuyBitcoins, String valueSellBitcoins)
	{
		this.valueBuyBitcoins=valueBuyBitcoins;
		this.valueSellBitcoins=valueSellBitcoins;
		this.data= new Date();
	}
	

}
