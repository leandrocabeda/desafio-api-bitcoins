package com.bitcoins.api.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.*;

@Data
@Entity
public class Investments {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(nullable = true)
	private String valueBuyBitcoins;
	
	
	@Column(nullable = true)
	private String valueInvestments ;
	
	
	@Column(name="data_buy")
	private Date data;
	
	
    @ManyToOne
    @JoinColumn(name="id_client",referencedColumnName="id", nullable = false)
	private Clients client;
    
    
    
    public Investments()
    {
    	
    }
	

}
