package com.bitcoins.api.entities;

import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import lombok.*;

@Data
@Entity
public class Clients implements UserDetails {
	
	
	private static final long serialVersionUID = 177414446647549495L;
	

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@NotEmpty(message = "Name cannot be empty")
	@NotBlank(message = "Name cannot be blank")
	@NotNull(message = "Name cannot be null")
	@Length(min = 5,message = "Name must be at least {min} characters")
	private String name;
	
	
	@NotEmpty(message = "Email cannot be empty")
	@NotBlank(message = "Email cannot be blank")
	@NotNull(message = "Email cannot be null")
	@Email(message = "Invalid email")
	@Column(unique = true)
	private String email;
	
	@NotEmpty(message = "Login cannot be empty")
	@NotBlank(message = "Login cannot be blank")
	@NotNull(message = "Login cannot be null")
	@Length(min = 5,message = "Login must be at least {min} characters")
	@Column(unique = true)
	private String login;
	
	
	@NotEmpty(message = "Password cannot be empty")
	@NotBlank(message = "Password cannot be blank")
	@NotNull(message = "Password cannot be null")
	@Length(min = 6,message = "Password must be at least {min} characters")
	private String password;
	
	
	@Column(nullable = true)
	private String value ;
	
	
	 @Override
	    public String getPassword() {
	        return password;
	    }

	    @Override
	    public String getUsername() {
	        return login;
	    }

	    @Override
	    public boolean isAccountNonExpired() {
	        return true;
	    }

	    @Override
	    public boolean isAccountNonLocked() {
	        return true;
	    }

	    @Override
	    public boolean isCredentialsNonExpired() {
	        return true;
	    }

	    @Override
	    public boolean isEnabled() {
	        return true;
	    }

		@Override
		public Collection<? extends GrantedAuthority> getAuthorities() {
			
			return null;
			
		}

}
