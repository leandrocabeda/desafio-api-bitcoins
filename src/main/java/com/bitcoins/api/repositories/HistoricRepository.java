package com.bitcoins.api.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import com.bitcoins.api.entities.Historic;


@Transactional(readOnly = true)
public interface HistoricRepository extends JpaRepository<Historic, Long> {

}
