package com.bitcoins.api.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import com.bitcoins.api.entities.Investments;;

@Transactional(readOnly = true)
public interface InvestmentsRepository extends JpaRepository<Investments, Long> {
	
	Investments findByClient(Long id);

}
