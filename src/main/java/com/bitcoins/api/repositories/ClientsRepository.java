package com.bitcoins.api.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import com.bitcoins.api.entities.Clients;

@Transactional(readOnly = true)
public interface ClientsRepository extends JpaRepository<Clients, Long> {
	
	Clients findByEmail(String email);
	
	Clients findByLogin(String login);
	
	Clients findByLoginOrEmail(String login,String email);

}
