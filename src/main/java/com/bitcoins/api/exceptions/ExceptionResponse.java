package com.bitcoins.api.exceptions;

import java.util.Date;

import lombok.*;

@Data
public class ExceptionResponse {

	private String message;
	private Date timestamp;
	private String details;
	
	
	public ExceptionResponse(String message, Date timestamp)
	{
		
		this.message=message;
		this.timestamp=timestamp;
	}
	
	public ExceptionResponse(String message, Date timestamp, String details)
	{
		
		this.message=message;
		this.timestamp=timestamp;
		this.details=details;
	}
	
	
}
