package com.bitcoins.api.exceptions;


import java.util.Date;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;


@RestControllerAdvice
public class ExceptionConfig extends ResponseEntityExceptionHandler {

	
	@ExceptionHandler({ 
		EmptyResultDataAccessException.class
	})
	public final ResponseEntity<?> errorNotFound(EmptyResultDataAccessException ex, WebRequest request)
	{
		return new ResponseEntity<Object>(
				new ExceptionResponse("Clients not found",
						new Date(), 
						request.getDescription(false)), 
				HttpStatus.NOT_FOUND);
	}
	
	
	@ExceptionHandler({
		IllegalArgumentException.class
	})
	public final ResponseEntity<?> errorBadRequest(IllegalArgumentException ex, WebRequest request)
	{
		
		return new ResponseEntity<Object>(
				new ExceptionResponse("Inappropriate method or argument.",
						new Date(), 
						request.getDescription(false)), 
				HttpStatus.NOT_FOUND);
	}
	
	@ExceptionHandler({
		AccessDeniedException.class
	})
	public final ResponseEntity<?> accessDenied()
	{
		
		return ResponseEntity.status(HttpStatus.FORBIDDEN).body(new ExceptionResponse("Access denied!",new Date()));
	}
	
	
	@Override
	protected ResponseEntity<Object> handleHttpRequestMethodNotSupported(
			HttpRequestMethodNotSupportedException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {

		return new ResponseEntity<Object>(
				new ExceptionResponse("Operation not allowed", new Date(), request.getDescription(false)), 
				HttpStatus.METHOD_NOT_ALLOWED);
	}
	
	
}

