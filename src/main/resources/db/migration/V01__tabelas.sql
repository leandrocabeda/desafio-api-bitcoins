create table clients(
id integer auto_increment not null,
name varchar(255) not null,
login varchar(255) not null unique,
email varchar(255) not null unique,
password varchar(255) not null,
value varchar(255),
primary key (id)
);


create table investments(
id integer auto_increment not null,
value_buy_bitcoins varchar(255),
value_investments varchar(255),
id_client integer not null,
data_buy date,
primary key (id),
CONSTRAINT fk_idClient FOREIGN KEY (id_client) REFERENCES clients (id) ON UPDATE CASCADE ON DELETE RESTRICT
);


create table historic(
id integer auto_increment not null,
value_buy_bitcoins varchar(255),
value_sell_bitcoins varchar(255),
data_historic date,
primary key (id)
);