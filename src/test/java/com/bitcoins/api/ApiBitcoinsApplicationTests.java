package com.bitcoins.api;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.bitcoins.api.entities.Clients;
import com.bitcoins.api.security.dto.JwtAuthenticationUser;
import com.bitcoins.api.security.dto.TokenUser;




@SpringBootTest(classes = ApiBitcoinsApplication.class,webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class ApiBitcoinsApplicationTests {

	
	@Autowired
	protected TestRestTemplate rest;
	
	
	@Test
	void CreateAndLoginClientOnApi() {
		
		Clients client = new Clients();
		client.setEmail("leo@hotmail.com");
		client.setLogin("leoCR");
		client.setName("leandro cabeda rigo");
		client.setPassword("123456");
		
		ResponseEntity<Clients> response =rest.postForEntity("/auth/createClients", client,null);
		
		assertEquals(HttpStatus.CREATED,response.getStatusCode());
		
		
		ResponseEntity<TokenUser> responseToken =rest.withBasicAuth("leoCR", "123456").
				postForEntity("/auth", new JwtAuthenticationUser("leoCR", "123456"), null);
		
		assertEquals(HttpStatus.OK,responseToken.getStatusCode());
		
		System.out.println("Token Client: \\r\\n"+responseToken.getBody());
		
		
	}
	

}
